package com.tuxar.fdum;



import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class RecoveryActivity extends Activity
{
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.recovery);
	}

	public void onRecoveryMainButtonClick (View View){
		TextView textView = (TextView) findViewById(R.id.recoveryTextView);
		textView.setText(R.string.recoveryTextMain);
	}

	public void onRecoveryPractButtonClick (View view){
		TextView textView = (TextView) findViewById(R.id.recoveryTextView);
		textView.setText(R.string.recoveryTextPractic);
	}

}
