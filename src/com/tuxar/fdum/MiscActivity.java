package com.tuxar.fdum;


import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.support.v7.app.ActionBarActivity;

public class MiscActivity extends ActionBarActivity  implements TabListener {
	

/*    MiscStartFragment miscStartFragment;

    private final String FRAGMENT_START = "FRAGMENT_START";
*/


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.misc);
        
    	ActionBar actionBar = getSupportActionBar();
    	actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
    	actionBar.setDisplayShowTitleEnabled(false);

    	Tab tab = actionBar.newTab()
    	                   .setText(R.string.vvedenieMainButtonName)
    	                   .setTabListener(this);
    	actionBar.addTab(tab);

    	tab = actionBar.newTab()
    	               .setText(R.string.vvedenieBootButtonName)
    	               .setTabListener(this);
    	actionBar.addTab(tab);

/*        if (miscStartFragment == null) {
        	miscStartFragment = new MiscStartFragment();
        }

        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.fragment_point2, miscStartFragment, FRAGMENT_START)
            .commit();
*/
    }

	@Override
	public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabSelected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
		// TODO Auto-generated method stub
		
	} 

}
