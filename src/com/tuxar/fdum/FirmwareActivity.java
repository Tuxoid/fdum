package com.tuxar.fdum;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class FirmwareActivity extends Activity
{
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.firmware);
	}

	public void onFirmwareMainButtonClick (View View){
		TextView textView = (TextView) findViewById(R.id.firmwareTextView);
		textView.setText(R.string.firmwareTextMain);
	}

	public void onFirmwarePractButtonClick (View view){
		TextView textView = (TextView) findViewById(R.id.firmwareTextView);
		textView.setText(R.string.firmwareTextPractic);
	}

}
