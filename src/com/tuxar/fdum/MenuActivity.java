package com.tuxar.fdum;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MenuActivity extends Activity implements OnItemClickListener {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);

		//String[] menu = new String[] { "Введение", "Взлом Bootloader'а", "Права root", "Кастомное Recovery",
		//		"Выбор прошивки", "Разное" };
		
/*		String[] menu = new String[] { 
			    getString(R.string.menu_vvedenie),
			    getString(R.string.menu_hack),
			    getString(R.string.menu_root),
			    getString(R.string.menu_recovery),
			    getString(R.string.menu_firmware),
			    getString(R.string.menu_misc)};
*/

        //Вытягиваем массив ссылок на строки из strings.xml
		String[] menu = getResources().getStringArray(R.array.menu_items);

		ListAdapter adapter = new MyAdapter(menu);

		ListView listView = (ListView) findViewById(R.id.menuListView1);
		listView.setOnItemClickListener(this);
		listView.setAdapter(adapter);

	}

	private class MyAdapter extends BaseAdapter {

		private String[] values;

		public MyAdapter(String[] values) {
			this.values = values;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.entry, parent, false);
			}

			String text = values[position];

			TextView textView = (TextView) convertView.findViewById(R.id.entryTextView1);
			ImageView imageView = (ImageView) convertView.findViewById(R.id.entryImageView1);

			textView.setText(text);
			imageView.setImageResource(android.R.drawable.ic_menu_info_details);

			switch (position) {
			case 0:
				imageView.setImageResource(android.R.drawable.ic_menu_gallery);
				break;

			case 1:
				// тут будут сетаться иконки для других айтемов
				break;

			default:
				break;
			}

			return convertView;
		}

		@Override
		public int getCount() {
			return values.length;
		}

		@Override
		public Object getItem(int position) {
			return values[position];
		}

		@Override
		public long getItemId(int position) {
			return values[position].hashCode();
		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

		Intent intent;

		switch (position) {
		case 0:
			intent = new Intent(MenuActivity.this, VvedenieActivity.class);
			startActivity(intent);
			break;

		case 1:
			// тут будут запуски других активити
			intent = new Intent(MenuActivity.this, HackActivity.class);
			startActivity(intent);
			break;
			
		case 2:
			intent = new Intent(MenuActivity.this, RootActivity.class);
			startActivity(intent);
			break;
			
		case 3:
			intent = new Intent(MenuActivity.this, RecoveryActivity.class);
			startActivity(intent);
			break;
			
			
		case 4:
			intent = new Intent(MenuActivity.this, FirmwareActivity.class);
			startActivity(intent);
			break;
			
			
		case 5:
			intent = new Intent(MenuActivity.this, MiscActivity.class);
			startActivity(intent);
			break;

		default:
		//Просто отладкка. Выводим номер тапнутой строки
			Toast.makeText(this, "Item clicked! " + position, Toast.LENGTH_SHORT).show();
			break;
		}

	}

}
