package com.tuxar.fdum;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class VvedenieActivity extends FragmentActivity {

    VvedenieFragmentStart vvedenieStartFragment;

    private final String FRAGMENT_START = "FRAGMENT_START";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vvedenie);

        if (vvedenieStartFragment == null) {
        	vvedenieStartFragment = new VvedenieFragmentStart();
        }

        getSupportFragmentManager()
            .beginTransaction()
            .replace(R.id.fragment_point, vvedenieStartFragment, FRAGMENT_START)
            .commit();

    }

}