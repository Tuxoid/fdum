package com.tuxar.fdum;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class HackActivity extends Activity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hack);
    }
    
    public void onHackMainButtonClick (View View){
    	TextView textView = (TextView) findViewById(R.id.hackTextView);
    	textView.setText(R.string.hackTextMain);
    }
    
    public void onHackPractButtonClick (View view){
    	TextView textView = (TextView) findViewById(R.id.hackTextView);
    	textView.setText(R.string.hackTextPractic);
    }

}
