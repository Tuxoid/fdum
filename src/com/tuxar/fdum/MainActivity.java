package com.tuxar.fdum;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		ImageView imageView = (ImageView) findViewById(R.id.mainPageImage);
		imageView.setImageResource(R.drawable.background);

		Toast.makeText(this, getString(R.string.start_toast), Toast.LENGTH_LONG).show();
	}

	public void onImageClick(View view) {
		Intent intent = new Intent(this, MenuActivity.class);
		startActivity(intent);
	}
}
