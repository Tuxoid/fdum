package com.tuxar.fdum;


import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

	public class RootActivity extends Activity
	{
	    /** Called when the activity is first created. */
	    @Override
	    public void onCreate(Bundle savedInstanceState)
		{
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.root);
	    }
	    
	    public void onRootMainButtonClick (View View){
	    	TextView textView = (TextView) findViewById(R.id.rootTextView);
	    	textView.setText(R.string.rootTextMain);
	    }
	    
	    public void onRootPractButtonClick (View view){
	    	TextView textView = (TextView) findViewById(R.id.rootTextView);
	    	textView.setText(R.string.rootTextPractic);
	    }

	}